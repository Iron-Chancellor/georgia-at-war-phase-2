## Local Development
1. From your install folder (not saved games), open scripts/MissionScripting.lua
2. Comment out all the lines in the do block below the sanitization function with "-\-".  This allows the lua engine access to the file system. It should look similar to:

        --sanitizeModule('os')
	    --sanitizeModule('io')
	    --sanitizeModule('lfs')
	    --require = nil
	    --loadlib = nil
3. Clone this repo.  From your `Saved Games\DCS.openbeta\Scripts` folder run `git clone https://gitlab.com/hoggit/developers/georgia-at-war-phase-2.git GAW2`.  This should create a folder named `GAW2` and in the end it should look like `Saved Games\DCS.openbeta\Scripts\GAW2
`
4. Download the .miz file from [Dropbox](https://www.dropbox.com/s/yhhp2hui9pvre69/Georgia_At_War_P2_v1.0.2.miz?dl=0)
